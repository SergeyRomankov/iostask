//
//  ViewController.m
//  RSTask1
//
//  Created by Сергей Романков on 04.02.16.
//  Copyright © 2016 Сергей Романков. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action
- (IBAction)action:(id)sender {
    // Создание объекта UIAlertView
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Hello World!" message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    // Показываем объект
    [alertView show];
}
@end
