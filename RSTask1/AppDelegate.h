//
//  AppDelegate.h
//  RSTask1
//
//  Created by Сергей Романков on 04.02.16.
//  Copyright © 2016 Сергей Романков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

